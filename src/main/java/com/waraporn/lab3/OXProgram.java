/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waraporn.lab3;

/**
 *
 * @author DELL
 */
class OXProgram {

    static boolean checkWin(String[][] board, String player) {
        if (checkRow(board, player)) {
            return true;
        }
        if (checkCol(board, player)) {
            return true;
        }
        if (checkX1(board, player)) {
            return true;
        }
        if (checkX2(board, player)) {
            return true;
        }
        if (checkDraw(board)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] board, String player) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(board, player, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] board, String player, int row) {
        return board[row][0].equals(player) && board[row][1].equals(player) && board[row][2].equals(player);
    }

    private static boolean checkCol(String[][] board, String player) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(board, player, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] board, String player, int col) {
        return board[0][col].equals(player) && board[1][col].equals(player) && board[2][col].equals(player);
    }

    private static boolean checkX1(String[][] board, String player) {
        if (board[0][0] == player && board[1][1] == player && board[2][2] == player) {
            return true;
        }
        return false;
    }

    private static boolean checkX2(String[][] board, String player) {
        if (board[0][2] == player && board[1][1] == player && board[2][0] == player) {
            return true;
        }
        return false;
    }

    private static boolean checkDraw(String[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == "_") {
                    return false;
                }
            }
        }
        return true;
    }

}
